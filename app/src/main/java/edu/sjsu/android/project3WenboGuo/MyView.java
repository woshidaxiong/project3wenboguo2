package edu.sjsu.android.project3WenboGuo;
import android.hardware.SensorManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.display.DisplayManager;
import android.view.Display;
import android.view.Surface;
import android.view.View;

import androidx.core.content.ContextCompat;

import edu.sjsu.android.project3WenboGuo.R;
public class MyView extends View implements SensorEventListener {

    // You can change the ball size if you want
    private static final int BALL_SIZE = 100;
    private Bitmap field;
    private Bitmap ball;
    private float XOrigin;
    private float YOrigin;
    private float horizontalBound;
    private float verticalBound;
    private final Particle mBall = new Particle();

    // Paint object is used to draw your name
    private Paint paint = new Paint();

    // TODO: set attributes (objects) needed for sensor
    // HINT: 2 of them are classes in the sensor framework
    //      1 is used for getting the rotation from "natural" orientation
    //      4 of them are used for the sensor data (3 axes + timestamp)
    private SensorManager sensorManager;
    private Sensor mSensor;
    private Display display;
    private float x; // data for x
    private float y;
    private float z;
    private long timestamp;

    public MyView(Context context) {
        super(context);
        DisplayManager displayManager;
        displayManager = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
        display = displayManager.getDisplay(Display.DEFAULT_DISPLAY);
        // You will see errors here because there are no image files yet.
        // Add the images under drawable folder to get rid of the errors.
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        ball = Bitmap.createScaledBitmap(b, BALL_SIZE, BALL_SIZE, true);
        field = BitmapFactory.decodeResource(getResources(), R.drawable.field);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }


    // HINT: there is a method in View class related to the size of the screen.
    //      The method's parameters include width and height of the screen.
    //      So override that method to set the 4 attributes.
    //      Also, you should createScaledBitmap for the basketball field,
    //      use the width and height of the screen in this method.

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        XOrigin = w / 2f;
        YOrigin = h/2f;
        horizontalBound = (w - BALL_SIZE) / 2F;
        verticalBound = (h - BALL_SIZE) / 2f;
        field = Bitmap.createScaledBitmap(field,w,h,true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the field and the ball
        canvas.drawBitmap(field, 0, 0, null);
        paint.setTextSize(50);
        paint.setColor(ContextCompat.getColor(this.getContext(),R.color.white));
        canvas.drawText("Wenbo Guo",100,100,paint);
        mBall.updatePosition(x,y,z,timestamp);
        mBall.resolveCollisionWithBounds(horizontalBound,verticalBound);
        canvas.drawBitmap(ball,
                (XOrigin - BALL_SIZE / 2f) + mBall.mPosX,
                (YOrigin - BALL_SIZE / 2f) - mBall.mPosY, null);

        invalidate();
    }

    public void startSimulation() {

        sensorManager.registerListener(this,mSensor,SensorManager.SENSOR_DELAY_UI);
    }

    public void stopSimulation() {
       sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Remember to interpret the data as discussed in Lesson 14 page 16.
        if(display.getRotation() == Surface.ROTATION_0){
            x = sensorEvent.values[0];
            y = sensorEvent.values[1];
        }else if(display.getRotation() == Surface.ROTATION_90){
            x = -sensorEvent.values[1];
            y = sensorEvent.values[0];
        }
        z = sensorEvent.values[2];
        timestamp = sensorEvent.timestamp;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}