package edu.sjsu.android.project3WenboGuo;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myView = new MyView(this);
        setContentView(myView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myView.startSimulation();

    }

    @Override
    protected void onStop() {
        super.onStop();
        myView.stopSimulation();
    }
}